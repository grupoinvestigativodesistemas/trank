ARG CENTOS_VERSION=7.6.1810
FROM centos:$CENTOS_VERSION as build

# TODO host this on the internal server?
RUN curl https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -o /miniconda-installer.sh
RUN bash /miniconda-installer.sh -b -p /python

RUN yum -y install postgresql-devel httpd-devel gcc
# gcc is needed by pip install, in particular to compile psycopg2,
# whose binary wheels are not recommended for production use.

# Set up venv
RUN /python/bin/python -m venv /venv && /venv/bin/pip install "mod_wsgi==4.6.*"

# Now install the app itself. Unlike the previous step, this step will
# be re-run whenever anything in this repo changes
COPY . /app
WORKDIR /app
RUN /venv/bin/pip install .


# Second stage

FROM centos:$CENTOS_VERSION

RUN yum -y install httpd postgresql-libs

COPY --from=build /python /python
COPY --from=build /venv /venv

# Overwrite the distro-provided httpd.conf with a pared-down
# single-application config.
COPY docker/trank/httpd.conf /etc/httpd/conf/httpd.conf
# This new config does not load the files from these directories, so
# remove them to avoid confusion.
RUN rm -r /etc/httpd/conf.d /etc/httpd/conf.modules.d

COPY trank-base.yaml.j2 docker/trank/trank.wsgi /app/

# make /run/httpd writeable by the apache user. This is bad security
# practice if running httpd as root, but we won't.
RUN chmod g+w /run/httpd

USER apache
CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]
