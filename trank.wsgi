import os, sys, site

os.environ["PATH"] = "/usr/local/datalib/bin" + ((":"+os.environ["PATH"]) if "PATH" in os.environ else "")
if "TRANK_CONFIG" not in os.environ:
   os.environ["TRANK_CONFIG"] = "/usr/local/datalib/var/trank/trank.yaml"

activate_this = '/usr/local/datalib/vpython3.7/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

#import ssl
#ssl._create_default_https_context = ssl._create_unverified_context

from trank.service import application

