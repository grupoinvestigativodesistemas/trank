CREATE ROLE trank WITH LOGIN PASSWORD 'SNIP';
CREATE DATABASE trank WITH OWNER=trank ENCODING='UTF8';
\connect trank
CREATE EXTENSION pgcrypto;
CREATE EXTENSION temporal_tables;
GRANT EXECUTE ON FUNCTION versioning() TO trank;

set trank.aid to 'admin';

set role trank;

BEGIN TRANSACTION;

DROP FUNCTION IF EXISTS sha1(text);
CREATE OR REPLACE FUNCTION sha1(s text) returns text AS $$
     SELECT encode(digest(s::bytea, 'sha1'), 'hex')
$$ LANGUAGE SQL STRICT IMMUTABLE;

DROP FUNCTION IF EXISTS generateId(text);
CREATE OR REPLACE FUNCTION generateId(s text = '') returns text AS $$
     SELECT sha1(s || now() || gen_random_uuid())
$$ LANGUAGE SQL STRICT IMMUTABLE;

DROP TABLE IF EXISTS players CASCADE;

DROP FUNCTION IF EXISTS versioning_2();
CREATE FUNCTION versioning_2() RETURNS TRIGGER AS $$ BEGIN NEW.ucreated = current_setting('trank.aid'); RETURN NEW; END; $$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS versioning_history();
CREATE FUNCTION versioning_history() RETURNS TRIGGER AS $$ BEGIN NEW.udeleted = current_setting('trank.aid'); RETURN NEW; END; $$ LANGUAGE plpgsql;


CREATE TABLE playersh (
   player_id TEXT NOT NULL, -- player id (sha-1)
   phone TEXT NOT NULL, -- cell phone number (unique)
   full_name TEXT NULL, -- full name
   email TEXT NULL, -- email address

   vrange tstzrange NOT NULL,
   ucreated text NOT NULL,
   udeleted text
);

CREATE TABLE players (
   PRIMARY KEY(player_id)
) INHERITS (playersh);

CREATE TRIGGER versioning_1 BEFORE INSERT OR UPDATE OR DELETE ON players FOR EACH ROW EXECUTE PROCEDURE versioning('vrange', 'playersh', true);
CREATE TRIGGER versioning_2 BEFORE INSERT OR UPDATE ON players FOR EACH ROW EXECUTE PROCEDURE versioning_2();
CREATE TRIGGER versioning_history BEFORE INSERT ON playersh FOR EACH ROW EXECUTE PROCEDURE versioning_history();


CREATE UNIQUE INDEX players_phone ON players (phone);

COMMIT TRANSACTION;
